from functools import wraps
from time import time
from hashlib import md5
import redis
import logging
from flask import request
try:
    import cPickle as pickle
except ImportError:  # pragma: no cover
    import pickle

integer_types = (int, )

logger = logging.getLogger(__name__)


class RedisCache(object):
    MODULE_PREFIX = "__cache__"
    COUNTER_PREFIX = "__counter__"

    def __init__(self, prefix="", expire=5, ttl=None, **kwargs):
        self.prefix = prefix + self.MODULE_PREFIX
        self.counter_prefix = prefix + self.COUNTER_PREFIX
        self.expire = expire
        # ttl is global key ttl, prevent redis mem leak, always useless
        self.ttl = ttl or expire * 100
        # init redis client
        self.redis = redis.StrictRedis(**kwargs)

    def _get_rel_key(self, key):
        return self.prefix + key

    def set_cache(self, key, value, expire=None):
        _expire = expire or self.expire
        _key = self._get_rel_key(key)
        _data = {"expiredIn": time() + _expire, "data": self.dump_object(value)}

        pipe = self.redis.pipeline()
        pipe.hmset(_key, _data)
        pipe.expire(_key, self.ttl)
        return pipe.execute()

    @staticmethod
    def is_valid_cache(cache):
        _expiredIn = cache.get(b"expiredIn")
        if _expiredIn is None:
            return False
        return float(_expiredIn) > time()

    def get_cache(self, key):
        return self.redis.hgetall(self._get_rel_key(key))

    @staticmethod
    def dump_object(value):
        t = type(value)
        if t in integer_types:
            return str(value).encode('ascii')
        return b'!' + pickle.dumps(value)

    @staticmethod
    def load_object(value):
        if value is None:
            return None
        if value.startswith(b'!'):
            try:
                return pickle.loads(value[1:])
            except pickle.PickleError:
                return None
        try:
            return int(value)
        except ValueError:
            return value

    def get_value(self, cache):
        return self.load_object(cache.get(b"data"))

    def incr(self, key):
        _key = self.counter_prefix + key
        c = self.redis.incr(_key)
        # for a new key, set ttl prevent redis mem leak, always useless
        if c == 1:
            self.redis.expire(_key, self.ttl)
        return c

    def decr(self, key):
        _key = self.counter_prefix + key
        c = self.redis.decr(_key)
        # delete the key when count is 0
        if c == 0:
            self.redis.delete(_key)
        return c


class Cache(object):
    def __init__(self, engine=RedisCache(), burst=None):
        self.engine = engine
        self.burst = burst

    def cached(self, expire=None, burst=None):
        burst = burst or self.burst

        def decorator(f):
            @wraps(f)
            def decorated_function(*args, **kwargs):
                _key = str(md5((request.path + request.method + _make_qs()).encode()).hexdigest())
                # try response cache, if not expire
                try:
                    _cache = self.engine.get_cache(_key)
                    rv = self.engine.get_value(_cache)
                    _data = rv
                    if rv is not None:
                        if self.engine.is_valid_cache(_cache):
                            logger.debug("hit cache")
                            return rv
                except Exception:
                    return f(*args, **kwargs)

                try:
                    c = self.engine.incr(_key)
                    # hit burst, return out of date cache anyway
                    if burst is not None and c > burst:
                        if _data is not None:
                            logger.debug("hit bust, return dirty cache")
                            return _data
                    # pass through, response and store cache
                    logger.debug("pass through")
                    rv = f(*args, **kwargs)
                    self.engine.set_cache(_key, rv, expire)
                    return rv
                finally:
                    self.engine.decr(_key)
                    pass

            def _make_qs():
                args_as_sorted_tuple = tuple(
                    sorted(
                        (pair for pair in request.args.items(multi=True))
                    )
                )
                return str(args_as_sorted_tuple)
            return decorated_function
        return decorator
