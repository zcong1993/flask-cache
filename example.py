from flask import Flask, jsonify
from fcache import Cache, RedisCache
import logging
import time

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
r = RedisCache(host="127.0.0.1", port="6379")
cache = Cache(engine=r)


@app.route('/hello/<name>/<words>', methods=['GET'])
@cache.cached(burst=2)
def hello(name, words):
    time.sleep(1)
    # visit http://127.0.0.1:5000/hello/haha/xs
    return jsonify({'name': name, 'words': words})


if __name__ == '__main__':
    app.run()
