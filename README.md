# flask-cache

## Example

```bash
$ pip install -r requirements.txt
$ python3 example.py
```

## Load test

use [k6](https://k6.io)

```bash
$ k6 run  --vus 10 -i 10 loadtest/k6.js
```