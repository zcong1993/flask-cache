import { check } from "k6";
import http from "k6/http";

const url = "http://127.0.0.1:5000/hello/haha/cdcd"

export default function() {
  let res = http.get(url);
  check(res, {
    "is status 200": (r) => r.status === 200
  });
}
